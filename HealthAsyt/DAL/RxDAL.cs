﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthAsyt.DAL
{
  public  class RxDAL
    {
        public int RxId { get; set; }

        public string PatientName { get; set; }
        public string DoctorName { get; set; }
        public string Symptoms { get; set; }
        public DateTime AppointmentDate { get; set; }


        public static List<RxDAL> RxDetails = new List<RxDAL>()
        {
            new RxDAL() { RxId=1, AppointmentDate=DateTime.Now, DoctorName="dr pavan" , PatientName="Rashmi", Symptoms="Fever" },
            new RxDAL() { RxId=2,AppointmentDate=DateTime.Now, DoctorName="dr pavan" , PatientName="Harshi", Symptoms="cold" },
            new RxDAL() { RxId=3,AppointmentDate=DateTime.Now, DoctorName="dr pavan" , PatientName="Ravi", Symptoms="caugh" },
            new RxDAL() { RxId=4,AppointmentDate=DateTime.Now, DoctorName="dr pavan" , PatientName="Sunil", Symptoms="Backpain" },
        };

        public void AddNewRX(DateTime appointmentDate, string doctorName, string patientName, string symptoms)
        {

            RxDetails.Add(new RxDAL() { RxId = RxDetails.Count + 1, AppointmentDate = appointmentDate, DoctorName = doctorName, Symptoms = symptoms });
        }

    }
}
