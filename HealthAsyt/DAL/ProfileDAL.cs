﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthAsyt.DAL
{
    public class ProfileDAL
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

     public static  List<ProfileDAL> userProfiles = new List<ProfileDAL>()
            {
                new ProfileDAL() { Name="rashmi", Email="rashmi@gmail.com ", Password="rash"},
                new ProfileDAL() {Name="Hars", Email="Hars@gmail.com ", Password="Hars" },
                new ProfileDAL() {Name="Ravi", Email="Ravi@gmail.com ", Password="Ravi" },
                new ProfileDAL() {Name="Pavan", Email="Pavan@gmail.com ", Password="Pavan" },
                new ProfileDAL() {Name="Sunil", Email="Sunil@gmail.com ", Password="Sunil" }

            };
        public void AddProfiles( string name, string email, string password)
        {
            userProfiles.Add(new ProfileDAL() { Name = name, Email = email, Password = password });
        }

        public bool ValidateUser(string userName, string password)
        {
           return userProfiles.Any(a => a.Name == userName && a.Password == password);
        }

    }

}
