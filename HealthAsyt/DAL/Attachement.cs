﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthAsyt.DAL
{
  public  class Attachement
    {
        public int RxId { get; set; }
        public int AttachementId { get; set; }
        public string AttachementFileName { get; set; }
    }
}
