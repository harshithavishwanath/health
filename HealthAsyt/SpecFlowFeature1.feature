﻿Feature: Add New Rx
	
@mytag
Scenario: Provide Rx Details 
	Given patient name , Symptoms, Doctor name, AppointmentDate
	When Click on save
	Then Success message should display "New Rx created successfully" with OK button
	And on clicking on "OK" take user to Home page
	And Saved Rx should be displayed in Home page

Scenario:  when no data entered by user
	Given no data entered by user
	When Click on save
	Then error message should display "Please enter mandatory fields" 
	And user should be retained in same page




