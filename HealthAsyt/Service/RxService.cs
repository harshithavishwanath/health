﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthAsyt.Service
{
    public class RxService
    {
        DAL.RxDAL dal = new DAL.RxDAL();

        public string AddNewRX(string doctorName, string patientName, string symptoms, DateTime appointmentDate)
        {
            if (string.IsNullOrEmpty(doctorName) || string.IsNullOrEmpty(patientName) || string.IsNullOrEmpty(symptoms))
                return "Please enter mandatory fields";
            else
            {
                dal.AddNewRX(appointmentDate, doctorName, patientName, symptoms);
                return "New Rx created successfully";
            }

        }
    }

}