﻿using System;
using TechTalk.SpecFlow;
using HealthAsyt.DAL;
using HealthAsyt.Service;
using NUnit.Framework;

namespace HealthAsyt
{
    [Binding]
    public class RegisterSteps
    {
        private string result = string.Empty;

        private ProfileDAL profile = new ProfileDAL();

        private ProfileService ps = new ProfileService();

        [Given(@"no data entered")]
        public void GivenNoDataEntered()
        {
            // ScenarioContext.Current.Pending();
            profile.Name = "";
            profile.Password = "";
            profile.Email = "";
        }
        
        [Given(@"username and password entered")]
        public void GivenUsernameAndPasswordEntered()
        {
           // ScenarioContext.Current.Pending();
        }
        
        [When(@"Click on sign up button")]
        public void WhenClickOnSignUpButton()
        {
            // ScenarioContext.Current.Pending();
            result = ps.AddNewRegistration(profile.Name, profile.Email, profile.Password);
        }
        
        [When(@"click on login button")]
        public void WhenClickOnLoginButton()
        {
           // ScenarioContext.Current.Pending();
        }
        
        [Then(@"an sucess message ""(.*)"" should be displayed")]
        public void ThenAnSucessMessageShouldBeDisplayed(string p0)
        {
          //  ScenarioContext.Current.Pending();
        }
        
        [Then(@"an error message saying ""(.*)""")]
        public void ThenAnErrorMessageSaying(string expectedResult)
        {
            // ScenarioContext.Current.Pending();
            Assert.AreEqual(expectedResult, result);
        }
        
        [Then(@"an error message saying ""(.*)"" should be displayed")]
        public void ThenAnErrorMessageSayingShouldBeDisplayed(string p0)
        {
           // ScenarioContext.Current.Pending();
        }
    }
}
