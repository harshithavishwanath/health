﻿Feature:  Register 

@mytag
Scenario: valid user data 
	Given enter username, password , email 
	When Click on sign up button
	Then an sucess message "Patient registered successfully" should be displayed

Scenario: try to register with no data 
	Given no data entered 
	When Click on sign up button
	Then an error message saying "Please enter mandatory fields"


Scenario: Login with invalid Patient id 
Given enter invalid patient id and valid password
When click on login button 
Then an error message saying "invalid user / password" should be displayed

Scenario: Login with valid data
Given username and password entered
When click on login button
Then sucess message saying "Logged in successfully" should be displayed



